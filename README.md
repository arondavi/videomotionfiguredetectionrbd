# VideoMotionFigureDetectionRBD

## Data

Z důvodu velikosti souborů a citlivých dat obsahující totožnost pacientů nejsou k práci tato data přiložena.
V místech chybějících dat jsou přiloženy README.txt soubory obsahující důvod odebrání a případný odkaz ke stažení některých necitlivých dat.
Vstupní a výstupní data k nahlédnutí a případnému otestování funkčnosti jsou k dispozici u vedoucího této práce.
