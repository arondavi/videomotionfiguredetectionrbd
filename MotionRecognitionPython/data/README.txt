Z důvodu citlivosti dat obsahujících totožnost pacientů byly odebrány snímky pacientů, na kterých byl tento model testován.
Snímky k nahlédnutí a případnému otestování funkčnosti jsou k dispozici u vedoucího této práce.

Z důvodu velikosti byl odebrán testovaný model. Ke stažení zde:
https://www.kaggle.com/code/billumillu/pose-estimation-pytorch/output