import os
import cv2


def gen_images_from_video(dir_path, video_file):
    cap = cv2.VideoCapture(dir_path + video_file)
    video_name = video_file.split('.')[0]
    fps = cap.get(cv2.CAP_PROP_FPS)

    scriptDir = os.path.dirname(__file__)
    outputFolder = f"{scriptDir}/{video_name.split('.')[0]}"

    if not os.path.exists(outputFolder):
        os.mkdir(outputFolder)

    has_frame, prev_frame = cap.read()
    for frame_idx in range(int(cap.get(cv2.CAP_PROP_FRAME_COUNT))):
        has_frame, frame = cap.read()
        if frame_idx % fps == 0:
            diff = abs(cv2.subtract(prev_frame, frame)).sum()
            print(str(round((frame_idx+1)*100/cap.get(cv2.CAP_PROP_FRAME_COUNT), 2))+"%")
            if diff>400000:
                file_name = f"{scriptDir}/{video_name}/{video_name}{str(frame_idx)}.jpg"
                cv2.imwrite(file_name, frame)
                prev_frame = frame

    cap.release()
    cv2.destroyAllWindows()


if __name__ == "__main__":
    dirPath = "D:/Programs/PythonProjects/DIPLOMKA/MotionRecognitionPython/data/videos/long/"
    videoFile = "patient1.wmv"
    gen_images_from_video(dirPath, videoFile)
