import cv2
import matplotlib.pyplot as plt
import torch
import torch.backends.cudnn as cudnn
import numpy as np
from scipy import signal as sig
import json

import pose_estimation as pe


def video_estimation(state_dict, cap):
    if not cap.isOpened():
        cap = cv2.VideoCapture(0)
        raise IOError("Cannot open the video")

    use_gpu = True

    model_pose = pe.get_pose_model()
    model_pose.load_state_dict(state_dict)

    if use_gpu:
        model_pose.cuda()
        model_pose = torch.nn.DataParallel(model_pose, device_ids=range(torch.cuda.device_count()))
        cudnn.benchmark = True

    fps = cap.get(cv2.CAP_PROP_FPS)
    max_frames = cap.get(cv2.CAP_PROP_FRAME_COUNT)
    width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
    size = (width, height)

    out = cv2.VideoWriter("annotated.avi", cv2.VideoWriter_fourcc(*"DIVX"), fps, size)
    has_frame = True
    count = 0
    while has_frame:
        has_frame, frame = cap.read()
        if not has_frame:
            cv2.waitKey()
            break
        count += 1
        print(str(count)+"/"+str(max_frames))
        frame = cv2.rotate(frame, cv2.ROTATE_90_COUNTERCLOCKWISE)
        try:
            annotated = pose_estimation_noprev(model_pose, frame)
            annotated = cv2.rotate(annotated, cv2.ROTATE_90_CLOCKWISE)
            out.write(annotated)
        except:
            print("Skipped")
    
    out.release()
    cap.release()


def video_estimation_better(state_dict, cap, showSignals):
    if not cap.isOpened():
        cap = cv2.VideoCapture(0)
        raise IOError("Cannot open the video")

    use_gpu = True

    model_pose = pe.get_pose_model()
    model_pose.load_state_dict(state_dict)

    if use_gpu:
        model_pose.cuda()
        model_pose = torch.nn.DataParallel(model_pose, device_ids=range(torch.cuda.device_count()))
        cudnn.benchmark = True

    fps = cap.get(cv2.CAP_PROP_FPS)
    max_frames = cap.get(cv2.CAP_PROP_FRAME_COUNT)
    width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
    size = (width, height)

    out = cv2.VideoWriter("annotated_better.avi", cv2.VideoWriter_fourcc(*"DIVX"), fps, size)
    has_frame = True
    count = 0
    all_points = []
    all_paf_info = []
    while has_frame:
        has_frame, frame = cap.read()
        if not has_frame:
            cv2.waitKey()
            break
        count += 1
        print(str(count)+"/"+str(max_frames))
        frame = cv2.rotate(frame, cv2.ROTATE_90_COUNTERCLOCKWISE)
        try:
            curr_paf_info, curr_points = pose_estimation_points(model_pose, frame)
            all_points.append(curr_points)
            all_paf_info.append(curr_paf_info)
        except:
            print("Skipped")
    
    for point in range(len(all_points[0])):
        part_X = []
        part_Y = []
        for frame in range(len(all_points)):
            if all_points[frame][point]:
                part_X.append(all_points[frame][point][0][0])
                part_Y.append(all_points[frame][point][0][1])

        filt_part_X = sig.medfilt(part_X, 15)
        filt_part_Y = sig.medfilt(part_Y, 15)

        if showSignals:
            figure, axis = plt.subplots(2, 2)
            axis[0, 0].plot(part_X)
            axis[0, 0].set_title("Před filtrací")
            axis[0, 1].plot(filt_part_X)
            axis[0, 1].set_title("Po filtraci")
            plt.show()

        used = 0
        for frame2 in range(len(all_points)):
            if all_points[frame2][point]:
                temp = list(all_points[frame2][point][0])
                temp[0] = filt_part_X[used]
                temp[1] = filt_part_Y[used]
                all_points[frame2][point][0] = tuple(temp)
                used += 1

    cap.set(cv2.CAP_PROP_POS_FRAMES, 0)
    index = 0
    has_frame = True
    while has_frame:
        has_frame, frame = cap.read()
        if not has_frame:
            cv2.waitKey()
            break
        frame = cv2.rotate(frame, cv2.ROTATE_90_COUNTERCLOCKWISE)
        image = pose_estimation_noprev_better(model_pose, frame, all_paf_info[index], all_points[index])
        image = cv2.rotate(image, cv2.ROTATE_90_CLOCKWISE)
        out.write(image)
        index += 1

    out.release()
    cap.release()


def video_estimation_points(state_dict, cap):
    if not cap.isOpened():
        cap = cv2.VideoCapture(0)
        raise IOError("Cannot open the video")

    use_gpu = True

    model_pose = pe.get_pose_model()
    model_pose.load_state_dict(state_dict)

    if use_gpu:
        model_pose.cuda()
        model_pose = torch.nn.DataParallel(model_pose, device_ids=range(torch.cuda.device_count()))
        cudnn.benchmark = True

    fps = cap.get(cv2.CAP_PROP_FPS)
    max_frames = cap.get(cv2.CAP_PROP_FRAME_COUNT)
    width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
    size = (width, height)

    has_frame = True
    count = 0
    all_points = []
    while has_frame:
        has_frame, frame = cap.read()
        if not has_frame:
            cv2.waitKey()
            break
        count += 1
        print(str(count)+"/"+str(max_frames))
        frame = cv2.rotate(frame, cv2.ROTATE_90_COUNTERCLOCKWISE)
        try:
            curr_points = pose_estimation_points(model_pose, frame)
            all_points.append(curr_points)
        except:
            print("Skipped")
    
    cap.release()
    return all_points;


def pose_estimation(state_dict, frame):
    use_gpu = True

    model_pose = pe.get_pose_model()
    model_pose.load_state_dict(state_dict)

    if use_gpu:
        model_pose.cuda()
        model_pose = torch.nn.DataParallel(model_pose, device_ids=range(torch.cuda.device_count()))
        cudnn.benchmark = True

    img_ori = frame

    scale_param = [0.5, 1.0, 1.5, 2.0]
    paf_info, heatmap_info = pe.get_paf_and_heatmap(model_pose, img_ori, scale_param)

    peaks = pe.extract_heatmap_info(heatmap_info)
    sp_k, con_all = pe.extract_paf_info(img_ori, paf_info, peaks)
    subsets, candidates = pe.get_subsets(con_all, sp_k, peaks)

    subsets, img_points = pe.draw_key_point(subsets, peaks, img_ori)
    img_canvas = pe.link_key_point(img_points, candidates, subsets)

    plt.figure(figsize=(12, 8))

    plt.subplot(1, 2, 1)
    plt.imshow(img_points[..., ::-1])

    plt.subplot(1, 2, 2)
    plt.imshow(img_canvas[..., ::-1])
    plt.show()

    return img_canvas


def pose_estimation_noprev(model_pose, frame):
    img_ori = frame

    scale_param = [0.5, 1.0, 1.5, 2.0]
    paf_info, heatmap_info = pe.get_paf_and_heatmap(model_pose, img_ori, scale_param)

    peaks = pe.extract_heatmap_info(heatmap_info)
    sp_k, con_all = pe.extract_paf_info(img_ori, paf_info, peaks)
    subsets, candidates = pe.get_subsets(con_all, sp_k, peaks)

    subsets, img_points = pe.draw_key_point(subsets, peaks, img_ori)

    img_canvas = pe.link_key_point(img_points, candidates, subsets)

    return img_canvas[..., ::-1]


def pose_estimation_noprev_better(model_pose, img_ori, paf_info, peaks):
    scale_param = [0.5, 1.0, 1.5, 2.0]

    sp_k, con_all = pe.extract_paf_info(img_ori, paf_info, peaks)
    subsets, candidates = pe.get_subsets(con_all, sp_k, peaks)

    subsets, img_points = pe.draw_key_point(subsets, peaks, img_ori)

    img_canvas = pe.link_key_point(img_points, candidates, subsets)

    return img_canvas[..., ::-1]


def pose_estimation_points(model_pose, frame):
    img_ori = frame

    scale_param = [0.5, 1.0, 1.5, 2.0]
    paf_info, heatmap_info = pe.get_paf_and_heatmap(model_pose, img_ori, scale_param)

    peaks = pe.extract_heatmap_info(heatmap_info)

    return paf_info, peaks