import cv2
import matplotlib.pyplot as plt
import torch
import numpy as np
from scipy import signal as sig
import os

import test01 as t1
import test02 as t2

scriptDir = os.path.dirname(__file__)

def pretest():
    pre_net = cv2.dnn.readNetFromTensorflow(f"{os.path.dirname(scriptDir)}/data/graph_opt.pb")
    img = cv2.imread(f"{os.path.dirname(scriptDir)}/data/foto.png")
    img = cv2.rotate(img, cv2.ROTATE_90_COUNTERCLOCKWISE)
    est_img = t1.pose_estimation(img, pre_net)
    plt.imshow(cv2.cvtColor(est_img, cv2.COLOR_BGR2RGB))
    plt.show()


def test01():
    pre_net = cv2.dnn.readNetFromTensorflow(f"{os.path.dirname(scriptDir)}/data/graph_opt.pb")
    videoPath = f"{os.path.dirname(scriptDir)}/data/videos/patient_move.mp4"
    t1.video_estimation(pre_net, videoPath)


def test02():
    state_dict = torch.load(f"{os.path.dirname(scriptDir)}/data/coco_pose_iter_440000.pth.tar")['state_dict']
    img = cv2.imread(f"{os.path.dirname(scriptDir)}/data/foto.png")
    frame = cv2.rotate(img, cv2.ROTATE_90_COUNTERCLOCKWISE)
    t2.pose_estimation(state_dict, frame)


def test02_video():
    state_dict = torch.load(f"{os.path.dirname(scriptDir)}/data/coco_pose_iter_440000.pth.tar")['state_dict']
    cap = cv2.VideoCapture(f"{os.path.dirname(scriptDir)}/data/videos/patient_move.mp4")
    t2.video_estimation(state_dict, cap)

def test02_video_points():
    # state_dict = torch.load(f"{os.path.dirname(scriptDir)}/data/coco_pose_iter_440000.pth.tar")['state_dict']
    # cap = cv2.VideoCapture(f"{os.path.dirname(scriptDir)}/videos/patient_move.mp4")
    # all_points = t2.video_estimation_points(state_dict, cap)

    # with open('patient_move', 'wb') as f:
    #     np.save(all_points) 

    with open('patient_move.npy', 'rb') as f:
        all_points = np.load(f)

    for point in range(len(all_points[0])):
        part_X = []
        part_Y = []
        for frame in range(len(all_points)):
            part_X.append(all_points[frame][point][0][0])
            part_Y.append(all_points[frame][point][0][1])
        part_X = sig.medfilt(part_X, 5)
        part_Y = sig.medfilt(part_Y, 5)
        for frame in range(len(all_points)):
            all_points[frame][point][0][0] = part_X[frame]
            all_points[frame][point][0][1] = part_Y[frame]
    
    print(all_points)


def test02_video_better():
    state_dict = torch.load(f"{os.path.dirname(scriptDir)}/data/coco_pose_iter_440000.pth.tar")['state_dict']
    cap = cv2.VideoCapture(f"{os.path.dirname(scriptDir)}/data/videos/patient_move.mp4")
    t2.video_estimation_better(state_dict, cap, True)


def main():
    # Tests of the small graph_opt.pb model
    # pretest()
    # test01()

    # Bigger OpenPose model tests
    # test02()
    # test02_video()
    # test02_video_points()
    test02_video_better()

    # t1.play_video()


if __name__ == "__main__":
    main()
