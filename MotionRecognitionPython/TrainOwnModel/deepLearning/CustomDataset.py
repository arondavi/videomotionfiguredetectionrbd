import json
import os

import cv2
import numpy as np
import torch
from torch.utils.data import Dataset

import utils as u


class CustomDataset(Dataset):
    def __init__(self, json_file, root_dir, transform=None):
        file = open(json_file)
        self.keypoints_frame = json.load(file)
        file.close()
        self.root_dir = root_dir
        self.transform = transform

    def __len__(self):
        return len(self.keypoints_frame)

    def __getitem__(self, idx):
        if torch.is_tensor(idx):
            idx = idx.tolist()

        img_name = self.keypoints_frame[idx]['img']
        img_name = img_name.split("-")[1]
        img_path = os.path.join(self.root_dir, img_name)
        image = cv2.imread(img_path)

        keypoints = self.keypoints_frame[idx]['kp-1']
        keypoints = np.asarray(keypoints)

        points = []
        for point in u.keypoints_def:
            found = False
            for keypoint in keypoints:
                if keypoint['keypointlabels'][0] == point:
                    points.append([keypoint['x'], keypoint['y']])
                    found = True
                    break
            if not found:
                points.append([0, 0])

        sample = {'image': image, 'keypoints': points}

        if self.transform:
            sample = self.transform(sample)

        return sample
