import cv2
import numpy as np
import torch


class Rescale(object):
    """ Rescale the image in a sample to a given size.

    Args:
        output_size (tuple or int): Desired output size. If tuple, output is
            matched to output_size. If int, smaller of image edges is matched
            to output_size keeping aspect ratio the same.
    """

    def __init__(self, output_size):
        assert isinstance(output_size, (int, tuple))
        self.output_size = output_size

    def __call__(self, sample):
        image, keypoints = sample['image'], sample['keypoints']

        if isinstance(self.output_size, int):
            new_w = self.output_size
            new_h = self.output_size
        else:
            new_w, new_h = self.output_size

        new_w, new_h = int(new_w), int(new_h)

        img = cv2.resize(image, (new_w, new_h))

        return {'image': img, 'keypoints': keypoints}


class RandomCrop(object):
    """ Crop randomly the image in a sample.

    Args:
        output_size (tuple or int): Desired output size. If int, square crop
            is made.
    """

    def __init__(self, output_size):
        assert isinstance(output_size, (int, tuple))
        if isinstance(output_size, int):
            self.output_size = (output_size, output_size)
        else:
            assert len(output_size) == 2
            self.output_size = output_size

    def __call__(self, sample):
        image, keypoints = sample['image'], sample['keypoints']

        h, w = image.shape[:2]
        new_h, new_w = self.output_size

        top = np.random.randint(0, h - new_h)
        left = np.random.randint(0, w - new_w)

        image = image[top: top + new_h, left: left + new_w]

        for i in range(len(keypoints)):
            image_w = len(image[0])
            image_h = len(image)

            x = keypoints[i][0] * (w / 100)
            y = keypoints[i][1] * (h / 100)

            if x - left < 0 or y - top < 0 or x - left > image_w or y - top > image_h:
                keypoints[i] = [0, 0]
            else:
                keypoints[i][0] = round((x - left) / (image_w / 100))
                keypoints[i][1] = round((y - top) / (image_h / 100))

        return {'image': image, 'keypoints': keypoints}


class ToTensor(object):
    """ Convert ndarrays in sample to Tensors """

    def __call__(self, sample):
        image, keypoints = sample['image'], sample['keypoints']

        if len(image.shape) == 2:
            # add that third color dim
            image = image.reshape(image.shape[0], image.shape[1], 1)

        image = image.transpose((2, 0, 1))
        return {'image': torch.tensor(image),
                'keypoints': torch.tensor(keypoints)}


class Normalize(object):
    """ Convert a color image to grayscale and normalize the color range to [0,1] """

    def __call__(self, sample):
        image, key_pts = sample['image'], sample['keypoints']

        image_copy = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)

        image_copy = image_copy / 255.0
        key_pts = np.asarray(key_pts) / 100

        return {'image': image_copy, 'keypoints': key_pts.tolist()}
