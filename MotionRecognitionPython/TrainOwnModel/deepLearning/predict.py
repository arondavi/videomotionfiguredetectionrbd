from torchvision import transforms
import torch
from CustomDataset import CustomDataset
from TransformOperations import *
import utils as u
from PIL import Image as im

if __name__ == '__main__':
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

    model = torch.load('TrainOwnModel/trainedModels/myPoseModel.pt', map_location=torch.device(device)).double()
    model.to(device)
    model.eval()

    data_transform = transforms.Compose([Rescale(250), RandomCrop(224), Normalize(), ToTensor()])
    transformed_dataset = CustomDataset(json_file='TrainOwnModel/data/annotations.json', root_dir='TrainOwnModel/data/kacafirek/',
                                        transform=data_transform)

    tens_img = [transformed_dataset[0]['image']]
    img = tens_img[0].numpy()
    img = np.transpose(img, (1, 2, 0))

    predict = model(torch.stack(tens_img).to(device))
    predict = predict.to('cpu')
    trans_predict = u.transform_predict_result(predict)

    print(trans_predict)

    u.show_landmarks(img, trans_predict)

