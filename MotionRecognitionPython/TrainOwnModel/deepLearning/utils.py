import cv2
import numpy as np

skeleton = [[0, 1], [1, 2], [2, 3], [3, 4], [1, 5], [5, 6], [6, 7], [1, 8], [8, 9], [9, 10], [1, 11],
            [11, 12], [12, 13], [0, 14], [0, 15], [14, 16], [15, 17]]
keypoints_def = ["nose", "neck", "right_shoulder", "right_elbow", "right_wrist", "left_shoulder", "left_elbow",
                 "left_wrist", "right_hip", "right_knee", "right_ankle", "left_hip", "left_knee", "left_ankle",
                 "right_eye", "left_eye", "right_ear", "left_ear"]


def show_landmarks(image, landmarks):
    if all(0 <= x[0] <= 1 and 0 <= x[1] <= 1 for x in landmarks):
        landmarks = (np.array(landmarks) * 100).tolist()

    for p in landmarks:
        if not p == [0, 0]:
            newp = [0, 0]
            newp[0] = round(p[0] * (len(image[0]) / 100))
            newp[1] = round(p[1] * (len(image) / 100))
            image = cv2.circle(image, (newp[0], newp[1]), radius=3, color=(0, 0, 255), thickness=-1)
            for pair in skeleton:
                if pair[0] == landmarks.index(p) and landmarks[pair[1]] != [0, 0]:
                    p2 = landmarks[pair[1]]
                    newp2 = [0, 0]
                    newp2[0] = round(p2[0] * (len(image[0]) / 100))
                    newp2[1] = round(p2[1] * (len(image) / 100))
                    image = cv2.line(image, (newp[0], newp[1]), (newp2[0], newp2[1]), color=(0, 255, 0), thickness=2)

    cv2.imshow("Image", image)
    cv2.waitKey()


def transform_predict_result(predict):
    predict = predict.detach().numpy()
    predict = predict[0]
    predict[predict < 0] = 0
    it = iter(predict)
    predict = [list(a) for a in zip(it, it)]
    return predict
