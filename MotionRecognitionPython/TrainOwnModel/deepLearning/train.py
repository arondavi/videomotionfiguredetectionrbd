import torch.optim as optim
from torch import nn
from torch.utils.data import DataLoader
from torchvision import transforms

from CustomDataset import CustomDataset
from Net import Net
from TransformOperations import *


def train_net(epochs, network, data_loader):

    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    network.to(device)

    # defining the loss criterion
    criterion = nn.MSELoss()
    # defining the optimizer method, for gradient descent
    optimizer = optim.Adam(network.parameters(), lr=0.0001)

    # prepare the net for training
    network.train()
    for epoch in range(epochs):  # loop over the dataset multiple times
        running_loss = 0.0
        # train on batches of data, assumes you already have train_loader
        for batch_i, data in enumerate(data_loader):
            # get the input images and their corresponding labels
            images = data['image']
            key_pts = data['keypoints']
            # flatten pts
            key_pts = key_pts.view(key_pts.size(0), -1)
            # convert variables to floats for regression loss
            key_pts = key_pts.type(torch.FloatTensor).to(device)
            images = images.type(torch.FloatTensor).to(device)
            # forward pass to get outputs
            output_pts = network(images)
            # calculate the loss between predicted and target keypoints
            loss = criterion(output_pts, key_pts)
            # zero the parameter (weight) gradients
            optimizer.zero_grad()
            # backward pass to calculate the weight gradients
            loss.backward()
            # update the weights
            optimizer.step()
            # print loss statistics
            running_loss += loss.item()
            if batch_i % 2 == 1:    # print every 2 batches
                print('Epoch: {}, Batch: {}, Avg. Loss: {}'.format(epoch + 1, batch_i+1, running_loss/10))
                running_loss = 0.0
    print('Finished Training')


if __name__ == "__main__":
    data_transform = transforms.Compose([Rescale(250), RandomCrop(224), Normalize(), ToTensor()])

    transformed_dataset = CustomDataset(json_file='TrainOwnModel/data/annotations.json', root_dir='TrainOwnModel/data/kacafirek/',
                                        transform=data_transform)

    batch_size = 5
    train_loader = DataLoader(transformed_dataset, batch_size=batch_size, shuffle=True, num_workers=4)

    net = Net()
    n_epochs = 10
    train_net(n_epochs, net, train_loader)
    torch.save(net, 'TrainOwnModel/trainedModels/myPoseModel.pt')
