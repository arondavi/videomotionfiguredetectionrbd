from torchvision import transforms

import SampleTransforms as st
import utils as u
from CustomDataset import CustomDataset
from TransformOperations import *
import numpy


def show_dataset():
    pose_dataset = CustomDataset(json_file='TrainOwnModel/data/annotations.json', root_dir='TrainOwnModel/data/kacafirek/')

    for i in range(len(pose_dataset)):
        sample = pose_dataset[i]
        u.show_landmarks(sample['image'], sample['keypoints'])

        print(sample['image'])

        sample = pose_dataset[i]
        sample_res = st.rescale(sample, 256)
        u.show_landmarks(sample_res['image'], sample_res['keypoints'])

        sample = pose_dataset[i]
        sample_crop = st.random_crop(sample, 180)
        u.show_landmarks(sample_crop['image'], sample_crop['keypoints'])

        sample = pose_dataset[i]
        sample_composed = st.composed_trans(sample, 250, 224)
        u.show_landmarks(sample_composed['image'], sample_composed['keypoints'])

        sample = pose_dataset[i]
        sample_norm = st.normalize(sample)
        u.show_landmarks(sample_norm['image'], sample_norm['keypoints'])


def show_dataset_transforms():
    transformed_dataset = CustomDataset(json_file='TrainOwnModel/data/annotations.json', root_dir='TrainOwnModel/data/kacafirek/',
                                        transform=transforms.Compose([
                                            Rescale(250), RandomCrop(224), ToTensor()
                                        ]))

    for i in range(len(transformed_dataset)):
        sample = transformed_dataset[i]

        print(sample['image'].numpy()[0])
        # cv2.imshow("asdasd",sample['image'].numpy()[0])
        # cv2.waitKey()
        # print(sample['keypoints'].numpy().tolist())

        u.show_landmarks(cv2.cvtColor(sample['image'].numpy()[0],cv2.COLOR_GRAY2RGB), sample['keypoints'].numpy().tolist())


if __name__ == "__main__":
    # show_dataset()
    show_dataset_transforms()
