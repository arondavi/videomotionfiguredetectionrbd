from torchvision import transforms

from TransformOperations import *


def rescale(sample, size):
    scale = Rescale(size)
    transformed_sample = scale(sample)
    return transformed_sample


def random_crop(sample, size):
    crop = RandomCrop(size)
    transformed_sample = crop(sample)
    return transformed_sample


def composed_trans(sample, size1, size2):
    composed = transforms.Compose([Rescale(size1), RandomCrop(size2), Normalize()])
    transformed_sample = composed(sample)
    return transformed_sample


def normalize(sample):
    norm = Normalize()
    transformed_sample = norm(sample)
    return transformed_sample
