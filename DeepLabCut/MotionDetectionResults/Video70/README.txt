Z důvodu citlivosti dat obsahujících totožnost pacientů byly odebrány videozáznamy pacientů, na kterých byl tento model testován.
Rovněž byly odebrány soubory Video70.mp4 a Video70_moves.mp4, které jsou výstupy analýzy a rovněž obsahují identitu pacienta.
Videozáznamy k nahlédnutí a případnému otestování funkčnosti jsou k dispozici u vedoucího této práce.