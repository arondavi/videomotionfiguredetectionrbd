Z důvodu citlivosti dat obsahujících totožnost pacientů byly odebrány výstupní snímky, které DeepLabCutPose exportuje.
Pro ukázku zde byly ponechány anonymizované ukázkové snímky pro síť EfficientNet.
Ostatní snímky jsou případně k nahlédnutí a otestování funkčnosti k dispozici u vedoucího této práce.