Z důvodu citlivosti dat obsahujících totožnost pacientů byly odebrány výstupní snímky, které zde DeepLabCutPose exportuje.
Pro ukázku zde byly ponechány anonymizované ukázkové snímky u videa 125.
Ostatní snímky jsou případně k nahlédnutí a otestování funkčnosti k dispozici u vedoucího této práce.