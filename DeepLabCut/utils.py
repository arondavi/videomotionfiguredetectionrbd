# Dev help functions

import matplotlib.pyplot as plt
import pandas as pd
import os


scriptDir = os.path.dirname(__file__)
cnns = ["ResNet-152", "MobileNet_v2_1.0", "EfficientNet-b6"] 


def showLoss():
    fig, axs = plt.subplots(1,3)
    fig.suptitle('Loss functions in CNNs', verticalalignment="bottom")
    fig.set_figwidth(12)
    fig.set_figheight(3)

    for shuffle in range(3):
        df = pd.read_csv(
            f"{scriptDir}/DeepLabCutPose-Aron-2023-11-13/dlc-models/iteration-0/DeepLabCutPoseNov13-trainset95shuffle{shuffle}/train/learning_stats.csv",
            header=None
        )

        axs[shuffle].plot(df.iloc[:, 0], df.iloc[:, 1])
        axs[shuffle].set_title(cnns[shuffle])
        axs[shuffle].set_xlabel("Iterations")
        axs[shuffle].set_ylabel("Loss")

        maxIter = df.iloc[-1, 0]
        axs[shuffle].set_xticks([0, maxIter/2, maxIter])
    
    plt.savefig(f"{scriptDir}/loss.png", bbox_inches='tight')


def plotDetectedMoves(signal, moves, outDir):
    plt.figure().set_figwidth(15)
    plt.ylim(0,5)
    plt.plot(signal)
    
    for move in moves:
        plt.plot(move, signal[move], color='r')

    plt.savefig(f"{outDir}/signal.png", bbox_inches='tight')
