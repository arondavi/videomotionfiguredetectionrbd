import tensorflow as tf
from tensorflow.python.client import device_lib
import tensorflow.keras as keras # type: ignore


def fix_gpu():
    config = tf.compat.v1.ConfigProto()
    config.gpu_options.allow_growth = True
    session = tf.compat.v1.InteractiveSession(config=config)


def check_capabilities():
    ## GPU Check
    fix_gpu()
    print(device_lib.list_local_devices())

    print("Num of GPUs Available: ", len(tf.config.experimental.list_physical_devices('GPU')))

    tf.test.is_built_with_cuda()

    print(tf.version.VERSION)

    print(tf.__version__)
    print(keras.__version__)


def local_devices_count():
    return len(device_lib.list_local_devices())


def main():
    check_capabilities()


if __name__ == "__main__":
    main()