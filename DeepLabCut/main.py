from utils import *
from motionDetection import *
import deeplabcut as dlc
from GPUChecks.gpuCheck import local_devices_count
import glob
import os


scriptDir = os.path.dirname(__file__)
config_path = f"{scriptDir}/DeepLabCutPose-Aron-2023-11-13/config.yaml"
outFolder = f"{scriptDir}/MotionDetectionResults"

if not os.path.isdir(outFolder):
    os.mkdir(outFolder)


def analyze_video(videoPath, shuffle):
    videoName = videoPath.split("/")[-1].split(".")[0]
    destFolder = f"{outFolder}/{videoName}"
    gpuToUse = None if local_devices_count() <= 0 else 0
    
    dlc.analyze_videos(config_path, videos=[videoPath], shuffle=shuffle, gputouse=gpuToUse, save_as_csv=True, destfolder=destFolder)
    dlc.filterpredictions(config_path, video=videoPath, shuffle=shuffle, filtertype="median", windowlength=51, save_as_csv=True, destfolder=destFolder)
    dlc.plot_trajectories(config_path, videos=[videoPath], shuffle=shuffle, destfolder=destFolder, imagetype=".png", resolution=300, filtered=True)
    result = dlc.create_labeled_video(config_path, videos=[videoPath], shuffle=shuffle, filtered=True, destfolder=destFolder, draw_skeleton=True)

    if all(result):
        renameFiles(videoName)
    
    return destFolder


def renameFiles(videoName):
    os.chdir(f"{outFolder}/{videoName}")

    # Rename video files
    videoFiles = glob.glob("*.mp4")
    if len(videoFiles) <= 0:
        raise Exception("No MP4 files! Please check analysis!")

    if len(videoFiles) == 1:
        os.rename(videoFiles[0], f"{outFolder}/{videoName}/{videoName}.mp4")
    else:
        for i in range(0, len(videoFiles)):
            os.rename(videoFiles[i], f"{outFolder}/{videoName}/{videoName}_{i}.mp4")
    
    # Rename csv files
    csvFiles = glob.glob("*filtered.csv")
    if len(csvFiles) <= 0:
        raise Exception("No CSV files! Please check analysis!")
    
    for file in csvFiles:
        os.rename(file, f"{outFolder}/{videoName}/{videoName}_filtered.csv")


def main():
    # showLoss()
    videoPath = f"{scriptDir}/DeepLabCutPose-Aron-2023-11-13/videos/Video70.wmv"

    outFolder = analyze_video(videoPath, 2)
    analyzeMotion(outFolder)


if __name__ == "__main__":
    main()