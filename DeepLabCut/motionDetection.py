import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import cv2 as cv
import os
from tqdm import tqdm
from utils import *
from datetime import timedelta
import statistics


thresholds = {
    "diff_limit": 0.4,
    "motion_search_limit": 150,
    "motion_frames_limit": 10,
    "intensity_medium_limit": 500,
    "intensity_strong_limit": 1000
}


bodyparts = {
  "head": "Head",
  "neck": "Neck",
  "R_shoulder": "Right shoulder",
  "L_shoulder": "Left shoulder",
  "R_elbow": "Right elbow",
  "L_elbow": "Left elbow",
  "R_wrist": "Right wrist",
  "L_wrist": "Left wrist",
  "R_hip": "Right hip",
  "L_hip": "Left hip",
  "R_knee": "Right knee",
  "L_knee": "Left knee",
  "R_foot": "Right foot",
  "L_foot": "Left foot"
}

intensityList = ["Low", "Medium", "Strong"]


def analyzeMotion(analyzedVideoDir):
    videoName = analyzedVideoDir.split("/")[-1]
    videoPath = f"{analyzedVideoDir}/{videoName}.mp4"
    csvPath = f"{analyzedVideoDir}/{videoName}_filtered.csv"

    data = pd.read_csv(csvPath, skiprows=1, header=[0,1])
    columns = list(set([x[0] for x in data.columns[1:]]))

    allMergedMoves = {}
    allSignals = {}

    for column in columns:
        signalX = np.trim_zeros(data[(column, 'x')].to_numpy())
        signalX = abs(np.diff(signalX))

        signalY = np.trim_zeros(data[(column, 'y')].to_numpy())
        signalY = abs(np.diff(signalY))
        
        allSignals[column] = (signalX, signalY)

        movesX = findMoves(signalX)
        movesY = findMoves(signalY)
        
        mergedMoves = mergeXYMoves(movesX, movesY)

        allMergedMoves[column] = mergedMoves

    exportMoves(videoPath, allMergedMoves, allSignals)
    writeMovesToVideo(videoPath, allMergedMoves)

    # plotDetectedMoves(signalX, movesX, analyzedVideoDir)


def mergeXYMoves(movesX, movesY):
    allMovesX = [x for xs in movesX for x in xs]
    allMovesY = [x for xs in movesY for x in xs]
    allMoves = list(set(allMovesX + allMovesY))
    allMoves.sort()

    return allMoves


def writeMovesToVideo(videoPath, allMergedMoves):
    outVideoPath = f"{os.path.dirname(videoPath)}/{videoPath.split('/')[-1].split('.')[0]}_moves.mp4"

    video = cv.VideoCapture(videoPath)
    maxFrames = video.get(cv.CAP_PROP_FRAME_COUNT)
    fps = video.get(cv.CAP_PROP_FPS)
    width = int(video.get(cv.CAP_PROP_FRAME_WIDTH))
    height = int(video.get(cv.CAP_PROP_FRAME_HEIGHT))
    size = (width, height)

    out = cv.VideoWriter(outVideoPath, cv.VideoWriter_fourcc(*"MP4V"), fps, size)
    font = cv.FONT_HERSHEY_SIMPLEX 
    has_frame = True
    frameCount = 0

    print("Writing moves to the video...")
    with tqdm(total=maxFrames) as pbar:
        while has_frame:
            has_frame, frame = video.read()
            
            if not has_frame:
                break
            
            frameCount += 1

            rowCount = 0
            for bodypart, moves in allMergedMoves.items():
                if frameCount in moves:
                    rowCount += 1
                    cv.putText(frame,f"{bodyparts[bodypart]} moved!",(5, (10+rowCount*13)),font, 0.4,(0, 0, 255),1)

            out.write(frame)
            pbar.update(1)


def exportMoves(videoPath, allMergedMoves, allSignals):
    outCSVPath = f"{os.path.dirname(videoPath)}/{videoPath.split('/')[-1].split('.')[0]}_moves.csv"
    
    video = cv.VideoCapture(videoPath)
    fps = video.get(cv.CAP_PROP_FPS)
    
    df = pd.DataFrame(columns=['StartTime', 'StopTime', 'BodyPart', 'Intensity'])

    for bodypart, moves in allMergedMoves.items():
        sepMoves = separate_intervals(moves)

        for sepMove in sepMoves:
            startSeconds = sepMove[0] / fps
            startTime = timedelta(seconds=startSeconds)

            stopSeconds = sepMove[-1] / fps
            stopTime = timedelta(seconds=stopSeconds)

            intensityLevel = getIntesityOfMove(sepMove, allSignals[bodypart])

            df.loc[len(df.index)] = [str(startTime), str(stopTime), bodyparts[bodypart], intensityList[intensityLevel]] 

    df = df.sort_values(by=['StartTime'])
    df.to_csv(outCSVPath, index=False)


def getIntesityOfMove(move, signals):
    moveX = signals[0][move]
    moveY = signals[1][move]

    usefullValuesX = moveX[moveX > thresholds["diff_limit"]]
    usefullValuesY = moveX[moveY > thresholds["diff_limit"]]

    diffLevel = 0
    if len(usefullValuesX) > 0: 
        diffLevel += statistics.mean(usefullValuesX)
    if len(usefullValuesY) > 0:
        diffLevel += statistics.mean(usefullValuesY)
    diffLevel *= 1000

    if diffLevel > thresholds["intensity_strong_limit"]:
        return 2
    elif diffLevel > thresholds["intensity_medium_limit"]:
        return 1
    else:
        return 0


def separate_intervals(nums):
    intervals = []
    current_interval = []

    for num in nums:
        if not current_interval or num == current_interval[-1] + 1:
            current_interval.append(num)
        else:
            intervals.append(current_interval)
            current_interval = [num]

    if current_interval:
        intervals.append(current_interval)

    return intervals


def findMoves(signalX):
    threshold = thresholds["diff_limit"]
    searchLimit = thresholds["motion_search_limit"]
    frameLimit = thresholds["motion_frames_limit"]

    intervals = []
    interval = []
    count = 0
    for idx, value in enumerate(signalX):
        if value > threshold:
            count = searchLimit
            interval.append(idx)
        else:
            if count > 0:
                interval.append(idx)
                count -= 1

        if count <= 0:
            interval = interval[:(1-searchLimit)]
            if len(interval) >= frameLimit:
                intervals.append(interval.copy())
            interval.clear()

    return intervals