import os
from pathlib import Path

import cv2
import matplotlib.pyplot as plt
from pycocotools.coco import COCO


def imgsToGray(imgs):
    for img in imgs:
        newFolder = './personGray/'
        filePath = dataDir + img['file_name']
        I = cv2.imread(filePath)
        I = cv2.cvtColor(I, cv2.COLOR_BGR2GRAY)

        if not os.path.exists(newFolder):
            os.makedirs(newFolder)

        cv2.imwrite(newFolder + img['file_name'], I)


def showAnnotatedImages(imgs):
    _, axs = plt.subplots(len(imgs), 2, figsize=(10, 5 * len(imgs)))
    for img, ax in zip(imgs, axs):
        filePath = dataDir + img['file_name']
        I = cv2.imread(filePath)
        I = cv2.cvtColor(I, cv2.COLOR_BGR2RGB)
        annIds = coco.getAnnIds(imgIds=[img['id']])
        anns = coco.loadAnns(annIds)
        ax[0].imshow(I, cmap='gray', vmin=0, vmax=255)
        ax[1].imshow(I, cmap='gray', vmin=0, vmax=255)
        plt.sca(ax[1])
        coco.showAnns(anns, draw_bbox=False)
        plt.show()

def showKeypointsInImage(imgs):
    for img in imgs:
        filePath = dataDir + img['file_name']
        I = cv2.imread(filePath)
        annIds = coco.getAnnIds(imgIds=[img['id']])
        anns = coco.loadAnns(annIds)
        print(anns)
        for ann in anns:
            for i in range(0, len(ann['keypoints']), 3):
                I = cv2.circle(I, (ann['keypoints'][i], ann['keypoints'][i+1]), radius=3, color=(0, 0, 255), thickness=-1)
            cv2.imshow("Keypoints", I)
        cv2.waitKey()

def showSkeletonInImage(imgs):
    for img in imgs:
        filePath = dataDir + img['file_name']
        I = cv2.imread(filePath)
        annIds = coco.getAnnIds(imgIds=[img['id']])
        anns = coco.loadAnns(annIds)

        skeleton = [[16, 14], [14, 12], [17, 15], [15, 13], [12, 13], [6, 12], [7, 13], [6, 7], [6, 8], [7, 9], [8, 10], [9, 11], [2, 3], [1, 2], [1, 3], [2, 4], [3, 5], [4, 6], [5, 7]]

        for ann in anns:
            print(ann['keypoints'])
            for i in range(0, len(ann['keypoints']), 3):
                if (ann['keypoints'][i] != 0) and (ann['keypoints'][i + 1] != 0):
                    I = cv2.circle(I, (ann['keypoints'][i], ann['keypoints'][i+1]), radius=3, color=(0, 0, 255), thickness=-1)

                    for j in range(0, len(skeleton)):
                        if skeleton[j][0] == (i / 3)+1:
                            if (ann['keypoints'][(skeleton[j][1]-1)*3] != 0) and (ann['keypoints'][(skeleton[j][1]-1)*3+1] != 0):
                                I = cv2.line(I, (ann['keypoints'][i], ann['keypoints'][i+1]), (ann['keypoints'][(skeleton[j][1]-1)*3], ann['keypoints'][(skeleton[j][1]-1)*3+1]), color=(0, 255, 0), thickness=2)

            cv2.imshow("Keypoints", I)
        cv2.waitKey()


if __name__ == "__main__":
    dataDir = "./person/"
    annFile = Path('./annotations/person_keypoints_train2017.json')
    coco = COCO(annFile)

    directory = 'person'
    imgIds = []
    for filename in os.listdir(directory):
        f = os.path.join(directory, filename)
        if os.path.isfile(f):
            imgIds.append(int(filename[6:-4]))

    imgs = coco.loadImgs(imgIds)

    #imgsToGray(imgs)
    #showAnnotatedImages(imgs)
    #showKeypointsInImage(imgs)
    showSkeletonInImage(imgs)