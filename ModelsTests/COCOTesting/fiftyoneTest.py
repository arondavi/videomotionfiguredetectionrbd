import fiftyone as fo
import fiftyone.zoo as foz


if __name__ == "__main__":
    dataset = foz.load_zoo_dataset(
        "coco-2017",
        split="train",
        label_types=["detections"],
        classes=["person"],
        max_samples=10,
    )

    session = fo.launch_app(dataset, desktop=True)
    session.wait()