from coco_dataset import coco_dataset_download as cocod
class_name='person'  #class name example
images_count=10       #count of images
annotations_path='annotations/person_keypoints_train2017.json' #path of coco dataset annotations
#call download function
cocod.coco_dataset_download(class_name,images_count,annotations_path)